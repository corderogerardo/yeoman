'use strict';

/**
 * @ngdoc function
 * @name yeomanyangularApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the yeomanyangularApp
 */
angular.module('yeomanyangularApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
